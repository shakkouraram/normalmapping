#ifndef  GRAPH
#define GRAPH
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "stb_image.h"

#include "graph.h"
#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include  "shader.h"
#include "camera.h"
#include "model.h"

#include <iostream>
class simple_graph
{
	int ** vertex_matrix;
	int num_of_vertices, num_of_edges;

public:
	simple_graph(int _num_of_vertices=5) : num_of_vertices(_num_of_vertices){
		num_of_edges = 0;
		vertex_matrix = new int*[num_of_vertices];
		for (int i = 0; i < _num_of_vertices; i++) {
			vertex_matrix[i] = new int[_num_of_vertices];
		}

		for (int i = 0; i < _num_of_vertices; i++) {
			for (int j = 0; j < _num_of_vertices; j++) {
				vertex_matrix[i][j] = INT_MAX;
			}
		}
	}

	~simple_graph() {
		for (int i = 0; i < num_of_vertices; i++) {
			delete[] vertex_matrix[i];
		}
		delete[] vertex_matrix;
	}

	std::vector<glm::vec3> getConnections() {
		std::vector<glm::vec3> cons(num_of_vertices);
		for (int i = 0; i < num_of_vertices; i++) {
			for (int j = 0; j < num_of_vertices; j++) {
				if (vertex_matrix[i][j] != INT_MAX) {
					glm::vec3 pos(i, j, vertex_matrix[i][j]);
					cons.push_back(pos);
				}
			}
		}
		cons.erase(cons.begin(), cons.begin() + num_of_vertices);
		return cons;
	}

	void add_edge(int v1, int v2, int weight) {
		if (vertex_matrix[v1 - 1][v2 - 1] == INT_MAX) {
			num_of_edges++;
		}
		vertex_matrix[v1 - 1][v2 - 1] = weight;
	}

	void del_edge(int v1, int v2) {
		if (vertex_matrix[v1 - 1][v2 - 1] != INT_MAX) {
			num_of_edges--;
		}
		vertex_matrix[v1 - 1][v2 - 1] = INT_MAX;
	}

	void print_graph() {
		for (int i = 0; i < num_of_vertices; i++) {
			for (int j = 0; j < num_of_vertices; j++) {
				if (vertex_matrix[i][j] != INT_MAX) {
					std::cout << vertex_matrix[i][j];
				}
				else {
					std::cout << "I";
				}std::cout << " ";
			}std::cout << std::endl;
		}
	}

};
#endif // ! GRAPH
